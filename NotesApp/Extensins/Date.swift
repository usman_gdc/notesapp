//
//  Date.swift
//  iOSApplication
//
//  Created by Usman Javaid on 28/02/2020.
//  Copyright © 2020 Ochobase. All rights reserved.
//

import Foundation

extension Date {
    
    func stringWith(Format _format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  _format
        return dateFormatter.string(from: self)
    }
    
//    func daysFrom(Date _endDate:Date) -> CGFloat {
//        let calendar = Calendar.current
//
//        let days = calendar.dateComponents([.day], from: self, to: _endDate).day ?? 0
//        if days == 0 {
//            return 1.0
//        }
//        return CGFloat(days)
//    }
}
