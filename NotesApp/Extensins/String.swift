//
//  String.swift
//  iOSApplication
//
//  Created by Usman Javaid on 18/11/2019.
//  Copyright © 2019 Ochobase. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    var length: Int {
        return count
    }
    
    var encodeURL: String {
        get {
            return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        }
    }

    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
    var empty: String {
        get {
            return ""
        }
    }
    
    var toInt: Int {
        get {
            return Int(self) ?? 0
        }
    }

    var toCGFloat: CGFloat {
        get {
            guard let _number = NumberFormatter().number(from: self), let _floatValue = CGFloat(exactly: _number) else {
                return 0.0
            }
            return _floatValue.rounded(toPlaces: 2)
        }
    }
    
    var trim: String {
        get {
            return self.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    
    var alphaNumeric: String {
        return components(separatedBy: CharacterSet.alphanumerics.inverted).joined()
    }
    
    var userProfileName: String {
        get {
            let words: [String] = self.components(separatedBy: " ")
            switch words.count {
            case 0:
                return ""
            case 1:
                return words[0].substring(toIndex: 2).uppercased()
            default:
                return "\(words[0].substring(toIndex: 1))\(words[1].substring(toIndex: 1))".uppercased()
            }
        }
    }
    
    func convertToDate(WithFormat _format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = _format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")

//        dateFormatter.timeZone = TimeZone.current
//        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: self)
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }

    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    func validateUsing(Regex _regex: String) -> Bool {
        let predicate = NSPredicate(format:"SELF MATCHES %@", _regex)
        return predicate.evaluate(with: self)
    }
    
//    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
//        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
//
//        return ceil(boundingBox.height)
//    }
//
//    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
//        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
//        return boundingBox.height
//    }
//
//    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
//
//        return ceil(boundingBox.width)
//    }
    
    func sizeOfText(constraintedWidth width: CGFloat, font: UIFont) -> CGSize {
       let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
       label.numberOfLines = 1
       label.lineBreakMode = NSLineBreakMode.byWordWrapping
       label.font = font
       label.text = self
       label.sizeToFit()
       return label.frame.size
    }
    
    func size(font: UIFont, width: CGFloat) -> CGSize {
        let attrString = NSAttributedString(string: self, attributes: [NSAttributedString.Key.font: font])
        let framesetter = CTFramesetterCreateWithAttributedString(attrString)
        let size = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRange(location: 0, length: self.count), nil, CGSize(width: width, height: .greatestFiniteMagnitude), nil)
        return size
    }
    
    
}
