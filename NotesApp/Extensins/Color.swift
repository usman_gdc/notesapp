//
//  Color.swift
//  NotesApp
//
//  Created by Muhammd Taha on 10/03/2021.
//

import Foundation
import UIKit

extension UIColor {
    
    class var primaryColor: UIColor {
        get {
            UIColor(named: "primary")!
        }
    }
    class var secondryColor : UIColor{
        get{
            UIColor(named: "secondry")!
        }
    }
    class var textColor: UIColor {
        get {
            UIColor(named: "textColor")!
        }
    }
}

class ThemeManager {
    static let primary: UIColor = UIColor(named: "primary")!
}
