//
//  UIViewController.swift
//  iOSApplication
//
//  Created by Usman Javaid on 14/11/2019.
//  Copyright © 2019 Ochobase. All rights reserved.
//

import Foundation
import UIKit
//import MBProgressHUD

extension UIViewController {
    
    
    class var storyboardID: String {
        return "\(self)"
    }
    
    static func initiateFrom(Storybaord _storybaord: Storyboard) -> Self {
        return _storybaord.viewController(Class: self)
    }
    
    //MARK:- Properties
    
    var isModal: Bool {

        let presentingIsModal = presentingViewController != nil
        let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
        let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController

        return presentingIsModal || presentingIsNavigation || presentingIsTabBar
    }
    
    var hideBackButton: Bool {
        get {
            return self.navigationItem.hidesBackButton
        }
        
        set {
            self.navigationItem.setHidesBackButton(true, animated:true)
            
        }
    }
    
    
    //MARK:- Methods
    
    
    
//    func showHud() -> MBProgressHUD {
//        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
//        progressHUD.mode = MBProgressHUDMode.indeterminate
//        progressHUD.detailsLabel.text = "Loading please wait..."
//////        progressHUD.margin = 10.0
//        progressHUD.isUserInteractionEnabled = true
//        progressHUD.removeFromSuperViewOnHide = true
//        return progressHUD
//    }
//
//    func hide(progressHUD _hud: MBProgressHUD) {
//        _hud.hide(animated: true)
//    }

    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
//    func showToastWith(_ message: String?) {
//        guard let _msg = message else {
//            return
//        }
//        let height: CGFloat = (UIScreen.main.bounds.height / 2.0) - 40
//        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
//        progressHUD.mode = MBProgressHUDMode.text
//        progressHUD.detailsLabel.text = _msg
//        progressHUD.margin = 10.0
//        progressHUD.offset.y = height
//        progressHUD.isUserInteractionEnabled = false
//        progressHUD.removeFromSuperViewOnHide = true
//        progressHUD.hide(animated: true, afterDelay: 3.0)
//    }
    
    func isPushed(From _viewContrller: UIViewController.Type) -> Bool {
        guard let _navController = self.navigationController, let previousController = _navController.getPreviousViewController() else {
            return false
        }
        return previousController.isKind(of: _viewContrller)
    }
    
    func isChildOf(parentViewContrller: UIViewController.Type) -> Bool {
        
        if let parentVC = self.parent {
            return parentVC.isKind(of: parentViewContrller)
        }
        return false
    }
    
    
    @discardableResult func popTo(ViewController _viewController: UIViewController.Type) -> Bool {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: _viewController.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        return false
    }
}
