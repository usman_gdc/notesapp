//
//  UIImage.swift
//  iOSApplication
//
//  Created by Usman Javaid on 10/03/2020.
//  Copyright © 2020 Ochobase. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func setImageColor(color: UIColor) {
      let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
      self.image = templateImage
      self.tintColor = color
    }
}
