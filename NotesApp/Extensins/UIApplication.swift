//
//  UIApplication.swift
//  Development
//
//  Created by Usman Javaid on 30/01/2020.
//  Copyright © 2020 Ochobase. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    
    var safeAreaTopInset: CGFloat {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.top ?? 0.0
        }
        return 0
    }
    
    var safeAreaBottomInset: CGFloat {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.bottom ?? 0.0
        }
        return 0
    }
}

