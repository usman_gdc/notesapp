//
//  UINavigationController.swift
//  iOSApplication
//
//  Created by Usman Javaid on 19/11/2019.
//  Copyright © 2019 Ochobase. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    //MARK:- Properties
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
       return topViewController?.preferredStatusBarStyle ?? .default
    }
    
    var barHeight: CGFloat {
        return (view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0.0) +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    //MARK:- Methdos
    
    func setTitle(title:String, WithFont font: UIFont, andColor color: UIColor ) {
        self.title = title
        self.setTitleFont(font: font, andColor: color)
    }
    
    func setTitleFont(font: UIFont, andColor color: UIColor ) {
        self.navigationBar.titleTextAttributes =
        [NSAttributedString.Key.foregroundColor: color,
         NSAttributedString.Key.font: font]
    }
    
    func setLeftBar(Button button: UIButton) {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    func setRightBar(Button button: UIButton) {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }

    func setLeftBarButton(WithImage name: String, selector: Selector) {
        let image: UIImage = UIImage(named: name)!
        let button : UIButton = UIButton(type: .custom)
        button.setImage(image, for: .normal)
//        button.leftImage(image: image, renderMode: .automatic)
        button.addTarget(self, action: selector, for: .touchUpInside)
        let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setLeftBarButtonItems([backBarButon], animated: false)
    }

    func setRightBarButton(WithImage name: String, selector: Selector) {
        let image: UIImage = UIImage(named :name)!
        let button : UIButton = UIButton(type: .custom)
        button.leftImage(image: image, renderMode: .automatic)
        button.addTarget(self, action: selector, for: .touchUpInside)
        let barButon: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setRightBarButtonItems([barButon], animated: true)
    }

    func getPreviousViewController() -> UIViewController? {
        let count = viewControllers.count
        guard count > 1 else { return nil }
        return viewControllers[count - 2]
    }
    
    func makeTransparent() {
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.navigationBar.tintColor = .clear
        self.interactivePopGestureRecognizer?.delegate = nil
//        self.navigationBar.tintColor = .clear
    }
    
    var hideNavigationBar: Bool {
            get {
                guard let _navigationController = self.navigationController else {
                    return false
                }
                return _navigationController.isNavigationBarHidden
            }
    
            set {
                self.setNavigationBarHidden(newValue, animated: true)
            }
        }
    
    
}
