//
//  NotesModel.swift
//  NotesApp
//
//  Created by Muhammd Taha on 09/03/2021.
//

import Foundation

enum NoteType: String, Codable {
    case text = "Text"
    case voice = "Voice"
    case drawing = "Image"
    
    var description: String {
        get {
            switch self {
            case .text:
                return "Text"
            case .voice:
                return "Voice"
            case .drawing:
                return "Image"
            }
        }
    }
}
struct NotesModel:Codable {
    
    var email: String?
    var name: String?
    var noteId: String?
    var note: String?
    var phoneNumber: String?
    var isArchive: Int16? 
    var noteType: NoteType?
    var mediaUrl: String?
    var isControl:Int16
    var noteAddress:String
    var addressUrl:String?
}
