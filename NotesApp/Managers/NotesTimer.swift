//
//  Timer.swift
//  NotesApp
//
//  Created by Muhammd Taha on 31/03/2021.
//

import Foundation

extension UserDefaults {
}

class NotesTimer {
    static let shared = NotesTimer()
    private var timer: Timer?
    private let totalTimeInSeconds: Int = 1200
    private var currentTimeInseconds: Int = 0
    private var isTimerAlreadyStart: Bool = false
    private let currentTimeInSecondsKey = "NOTES_CURRENT_TIME_IN_SECONDS_KEY"
    private let notesExpireDateKey = "NOTES_24_DATE__KEY"
    public static var remaingTime:String = ""
    private init() {
        
    }
    
    private func getTimerCurrentSeconds() {
        guard let _value = UserDefaults.standard.value(forKey: currentTimeInSecondsKey) as? Int else {
            currentTimeInseconds = 0
            return
        }
        currentTimeInseconds = _value
    }
    
    func checkTimerIsLocked() {
        guard let _previousDate = UserDefaults.standard.value(forKey: notesExpireDateKey) as? Date else {
            getTimerCurrentSeconds()
            return
        }
        let calendar1 = Calendar.current
         let components = calendar1.dateComponents([.second], from:  _previousDate, to:   Date())
        guard let _seconds = components.second, (_seconds - 86400) > 0 else {
            //_seconds - 86400
            self.currentTimeInseconds = totalTimeInSeconds
            self.setTimerCurrentSeconds()
            return
        }
        self.currentTimeInseconds = 0
        self.setTimerCurrentSeconds()
        self.setLock(isLock: false)
    }
    
    func setTimerCurrentSeconds() {
        UserDefaults.standard.setValue(currentTimeInseconds, forKey: currentTimeInSecondsKey)
    }
    
    func setLock(isLock: Bool) {
        if isLock {
            UserDefaults.standard.setValue(Date(), forKey: notesExpireDateKey)
            return
        }
        UserDefaults.standard.setValue(nil, forKey: notesExpireDateKey)
    }
    
    func startTimer(_ compltion: @escaping(_ value: String, _  isExpire: Bool) -> Void) {
        
        guard self.isTimerAlreadyStart == false else { return }
        
        if self.currentTimeInseconds >= self.totalTimeInSeconds {
            let result = self.timeFrom(seconds: self.totalTimeInSeconds)
            compltion("\(result.minute ) : \(result.seconds)", true)
            self.stop()
            return
        }
        
        self.isTimerAlreadyStart = true
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { _ in
            self.currentTimeInseconds = self.currentTimeInseconds  + 1
            self.setTimerCurrentSeconds()
            if self.currentTimeInseconds >= self.totalTimeInSeconds {
                let result = self.timeFrom(seconds: self.totalTimeInSeconds)
                compltion("\(result.minute ) : \(result.seconds)", true)
                self.stop()
                self.setLock(isLock: true)
                return
            }
            let result = self.timeFrom(seconds: self.totalTimeInSeconds - self.currentTimeInseconds)
            compltion("\(result.minute):\(result.seconds)", false)
            print(" the time is \(result.minute):\(result.seconds)")
            NotesTimer.remaingTime = "\(result.minute):\(result.seconds)"
        })
    }
    func stop() {
        self.isTimerAlreadyStart = false
        guard let _timer = self.timer else { return }
        _timer.invalidate()
        self.timer = nil
    }
    private func timeFrom(seconds: Int) -> (minute: Int, seconds: Int) {
        return (Int(seconds / 60) , Int(seconds % 60))
    }
}
