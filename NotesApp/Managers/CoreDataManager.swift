//
//  CoreDataManager.swift
//  iOSApplication
//
//  Created by Muhammd Taha on 09/03/2021.
//

import Foundation
import CoreData

class CoreDataManager {
    
    // MARK: - Core Data stack
    lazy fileprivate var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "NotesCoreData")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    static let manager: CoreDataManager = CoreDataManager()
    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    private init() {
    }
    
    // MARK: - Core Data Saving support
    func saveContext () {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    //    //MARK:- My Cart
    //        func addItemToCart(model: Person) throws {
    //            let notesObject = NSEntityDescription.insertNewObject(forEntityName: "id", into: context)
    //
    //            notesObject.setValue(model.number, forKey: "number")
    //            notesObject.setValue(model.email, forKey: "email")
    //            notesObject.setValue(model.name, forKey: "name")
    //            notesObject.setValue(model.note, forKey: "note")
    //            do {
    //                try context.save()
    //            } catch {
    //                print("Context not saved")
    //            }
    //
    //        }
    //
    //    func getAllCarts() throws -> [Person] {
    //        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MyCart")
    //        fetchRequest.resultType = .dictionaryResultType
    //        do {
    //            guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: []) else {
    //                print("Error ")
    //            }
    //            guard let model = try? JSONDecoder().decode([Persons].self, from: jsonData) else {
    //                print("Error to mapping")
    //            }
    //            return model
    //        }
    //        catch {
    //            print("Error Found")
    //        }
    //    }
    //
    //    func deleteCartBy(cartid: String?) throws {
    //        guard let _cartid = cartid else {
    //            print("Error here")
    //        }
    //
    //        let fetchRequest: NSFetchRequest<> = MyCart.fetchRequest()
    //        fetchRequest.predicate = NSPredicate(format: "cartId == %@", _cartid)
    ////        fetchRequest.predicate = Predicate.init(format: "profileID==\(withID)")
    //        let objects = try! context.fetch(fetchRequest)
    //        for obj in objects {
    //            context.delete(obj)
    //        }
    //
    //        do {
    //            try context.save() // <- remember to put this :)
    //        } catch {
    //            print("Error found")
    //        }
    //    }
    //
    //    func updateCartProductBy(cartid: String?, productQuantity: Int) throws {
    //            guard let _cartid = cartid else {
    //                print("Error found")
    //            }
    //
    //            let fetchRequest: NSFetchRequest<MyCart> = MyCart.fetchRequest()
    //            fetchRequest.predicate = NSPredicate(format: "cartId == %@", _cartid)
    //    //        fetchRequest.predicate = Predicate.init(format: "profileID==\(withID)")
    //            guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
    //                print("Error found")
    //                }
    //            _first.quantity = Int16(productQuantity)
    //            do {
    //                try context.save() // <- remember to put this :)
    //            } catch {
    //                print("Error found")
    //            }
    //        }
    //
    //    func deleteAllCartItems() throws {
    //        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MyCart")
    //        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
    //        do {
    //            try context.execute(deleteRequest)
    //        } catch {
    //            print("Error found")
    //        }
    //    }
    //
    //    //MARK:- Saved Addresses
    //    func saveAllAddress(addresses: [SavedAddressModel]) throws {
    //
    //        for address in addresses {
    //            let savedAddressObject = NSEntityDescription.insertNewObject(forEntityName: "SavedAddresses", into: context)
    //            savedAddressObject.setValue(address.id, forKey: "id")
    //            savedAddressObject.setValue(address.address, forKey: "address")
    //        }
    //        do {
    //            try context.save()
    //        } catch {
    //            print("Error found")
    //        }
    //    }
    //
    //    func deleteAllAddress() throws {
    //        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedAddresses")
    //        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
    //
    //        do {
    //            try context.execute(deleteRequest)
    //        } catch {
    //            print("Error found")
    //        }
    //    }
    //
    //    func getAllAddress() throws -> [SavedAddressModel] {
    //        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedAddresses")
    //        fetchRequest.resultType = .dictionaryResultType
    //        do {
    //            guard let results = try? context.fetch(fetchRequest),
    //            let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted),
    //             let model = try? JSONDecoder().decode([SavedAddressModel].self, from: jsonData) else {
    //                print("Error found")
    //            }
    //            return model
    //        }catch {
    //            print("Error found")
    //        }
    //    }
    //
    //    func deleteAddressBy(addressId: Int) throws {
    //
    //            let fetchRequest: NSFetchRequest<SavedAddresses> = SavedAddresses.fetchRequest()
    //            fetchRequest.predicate = NSPredicate(format: "id == %d", addressId)
    //            let objects = try! context.fetch(fetchRequest)
    //            for obj in objects {
    //                context.delete(obj)
    //            }
    //
    //            do {
    //                try context.save() // <- remember to put this :)
    //            } catch {
    //                print("Error found")
    //            }
    //        }
}

extension CoreDataManager {
    
    //MARK:- Notes Functions
    func addNote(note: NotesModel) throws {
        
        let notesObject = NSEntityDescription.insertNewObject(forEntityName: "Notes", into: context)
        notesObject.setValue(note.email, forKey: "email")
        notesObject.setValue(note.noteId, forKey: "noteId")
        notesObject.setValue(note.name, forKey: "name")
        notesObject.setValue(note.note, forKey: "note")
        notesObject.setValue(note.phoneNumber, forKey: "phoneNumber")
        notesObject.setValue(note.isArchive, forKey: "isArchive")
        notesObject.setValue(note.noteType?.description, forKey: "noteType")
        notesObject.setValue(note.mediaUrl, forKey: "mediaUrl")
        notesObject.setValue(note.isControl, forKey: "isControl")
        notesObject.setValue(note.noteAddress, forKey: "noteAddress")
        notesObject.setValue(note.addressUrl, forKey: "addressUrl")
        do {
            try context.save()
            print("Context Saved" , context)
        } catch {
            print("Exception while saving")
            throw ErrorTypes.notSaved
        }
    }
    func getAllNotes() throws -> [NotesModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
        fetchRequest.predicate = NSPredicate(format: "isArchive == 0")
        fetchRequest.resultType = .dictionaryResultType
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([NotesModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        print("Model is ",model)
        return model
    }
    func getArchiveNotes() throws -> [NotesModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
        fetchRequest.predicate = NSPredicate(format: "isArchive == 1")
        fetchRequest.resultType = .dictionaryResultType
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([NotesModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        print("Model is ",model)
        return model
    }
    
    func deleteNoteBy(noteId: String) throws {
        
        let fetchRequest: NSFetchRequest<Notes> = Notes.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "noteId == %@", noteId)
        let objects = try! context.fetch(fetchRequest)
        for obj in objects {
            context.delete(obj)
        }
        do {
            try context.save() // <- remember to put this :)
        } catch {
            print("Error found")
        }
    }
    
    func updateNote(note: NotesModel) throws {
        
        let fetchRequest: NSFetchRequest<Notes> = Notes.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "noteId == %@", note.noteId!)
        guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        _first.name = note.name
        _first.email = note.email
        _first.phoneNumber = note.phoneNumber
        _first.note = note.note
        _first.mediaUrl = note.mediaUrl
        _first.isControl = note.isControl
        _first.noteAddress = note.noteAddress
        _first.addressUrl = note.addressUrl
        
        do {
            try context.save() //<-remember to put this :)
        } catch {
            print("Error found")
            throw ErrorTypes.notFound
        }
    }
    func updateArchive(note: NotesModel) throws {
        let fetchRequest: NSFetchRequest<Notes> = Notes.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "noteId == %@", note.noteId!)
        guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        _first.isArchive = note.isArchive!
        do {
            try context.save() //<-remember to put this 
        } catch {
            print("Error found")
            throw ErrorTypes.notFound
        }
    }
    
    func updatearch(noteId : String?, isArchive:Int) throws{
        guard let _noteId = noteId else{
            return
        }
        let fetchRequest: NSFetchRequest<Notes> = Notes.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "noteId == %@", _noteId)
        guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
                       return}
        _first.isArchive = Int16(isArchive)
        do {
                        try context.save() // <- remember to put this :)
                    } catch {
                        print("Error found")
                    }
    }
}
