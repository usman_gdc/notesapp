//
//  EditDrawingViewController.swift
//  NotesApp
//
//  Created by Muhammd Taha on 12/03/2021.
//

import UIKit

class EditDrawingMainViewController: UIViewController {
    //MARK:- Outlets
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtMail: UITextField!
    @IBOutlet weak var txtNote: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageBtn: UIButton!
    @IBOutlet weak var uiSwitch: UISwitch!
    @IBOutlet weak var controlLbl: UILabel!
    
    
    //MARK:- Initializations
    var control:Int16!
    var data:NotesModel!
    var imageUrl : String!
    
    var imageString :String?
    //MARK:- LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        getValue()
        getImage()
        setBackButton(WithImage: "back24p")
        self.control = data.isControl
        navigationItem.title = "Edit Drawing Note"
    }
    
    //MARK:- Actions
    @IBAction func uiSwitch(_ sender: UISwitch) {
        if sender.isOn{
            self.controlLbl.text = "Yes"
            control = 1
        }
        else{
            self.controlLbl.text = "No"
            control = 0
        }
    }
    @IBAction func btnSave(_ sender: Any) {
        saveImageToDocumentDirectory(image: imageView.image!)
        self.navigationController?.popViewController(animated: true)
        setValues()
    }
    @IBAction func btnImage(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(identifier: "EditDrawingViewController") as! EditDrawingViewController
        viewController.delegate = self
       // viewController.mainImageView = self.imageView
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //MARK:- Functions
    func setBackButton(WithImage name: String) {

        navigationItem.leftBarButtonItem = nil
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 80, height: 31)
        button.setImage(UIImage(named: name), for: .normal)
        button.contentHorizontalAlignment = .leading
        button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
        button.tintColor = UIColor.white
        navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: button)]
    }
    @objc func backBtn(){
        self.navigationController?.popViewController(animated: true)
    }
    func getValue(){
        self.txtName.text = data.name
        self.txtName.textColor = UIColor.primaryColor
        self.txtMail.text = data.email
        self.txtMail.textColor = UIColor.primaryColor
        self.txtNote.text = data.note
        self.txtNote.textColor = UIColor.primaryColor
        self.txtPhone.text = data.phoneNumber
        self.txtPhone.textColor = UIColor.primaryColor
        self.imageUrl = data.mediaUrl
        if data.isControl == 1{
            controlLbl.text = "YES"
            uiSwitch.isOn = true
        }
        else {
            controlLbl.text = "NO"
            uiSwitch.isOn = false
        }
    }
    func setValues(){
        data.name = txtName.text!
        data.email = txtMail.text!
        data.phoneNumber = txtPhone.text!
        data.note = txtNote.text
        data.mediaUrl = imageString
        data.isControl = control
        editNote(note: data)
        
    }
    func getImage(){
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first{
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(imageUrl!)
            let image = UIImage(contentsOfFile: imageURL.path)
            imageView.image = image
        }
    }
    func saveImageToDocumentDirectory(image: UIImage) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileName = NSUUID().uuidString + ".png"
        imageString = fileName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        print("File PAth is ",fileURL)
        if let data = image.jpegData(compressionQuality: 1.0),!FileManager.default.fileExists(atPath: fileURL.path){
            do {
                try data.write(to: fileURL)
                
                print("Path of file as string is ", self.imageString!)
            } catch {
                print("error saving file:", error)
            }
        }
    }
    func editNote(note:NotesModel){
        do {
            try CoreDataManager.manager.updateNote(note: note)
            print("Note updated Succcessfully")
        }catch{
            print("Error updating note")
        }
    }
}
//MARK:- Extensions

extension EditDrawingMainViewController : Recieve{
    func rceieve(image: UIImage) {
        self.imageView.image = image
        print("Image string",image)
    }
    
    
}

