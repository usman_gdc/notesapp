//
//  DrawingViewController.swift
//  NotesApp
//
//  Created by Muhammd Taha on 08/03/2021.
//

import UIKit

protocol CanReceive {
    func dataReceive(image:UIImage)
}

class AddDrawingViewController: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var timerLbl: UILabel!
    @IBOutlet weak var tempImageView: UIImageView!
    
    
    
    //MARK:- Initializations
    var delegate:CanReceive!
    var lastPoint = CGPoint.zero
    var color = UIColor.black
    var brushWidth: CGFloat = 10.0
    var opacity: CGFloat = 1.0
    var swiped = false
    var hours: Int = 0
    var mins: Int = 0
    var secs: Int = 0
    var rightBarButton: UIBarButtonItem!
    var timerTitle: String? = nil {
        didSet {
            guard let _rightBarButton = self.rightBarButton else {
                rightBarButton = UIBarButtonItem.init(title: self.timerTitle ?? "00:00", style: .plain, target: self, action: nil)
                self.navigationItem.rightBarButtonItems =  [rightBarButton]
                return
            }
            _rightBarButton.title = timerTitle ?? "00:00"
        }
    }
    
    //MARK:- LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        //notification()
        setBackButton(WithImage: "back24p")
        timerTitle = nil
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        startNotesTimer()
    }
    //MARK:- Actions
    @IBAction func btnSettings(_ sender: Any) {
        //NotesTimer.shared.stop()
        let viewController = self.storyboard?.instantiateViewController(identifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.present(viewController, animated: true, completion: nil) 
        viewController.delegate = self
        viewController.brush = brushWidth
        viewController.opacity = opacity
    }
    @IBAction func resetPressed(_ sender: Any) {
        mainImageView.image = nil
    }
    
    @IBAction func sharePressed(_ sender: Any) {
        NotesTimer.shared.stop()
        guard let image = mainImageView.image else {
            let alertController = UIAlertController(title: "Empty Image?", message: "Write something to save", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                
                
            }
            // Add the actions
            alertController.addAction(okAction)
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            return
        }
        self.delegate.dataReceive(image: image)
        self.navigationController?.popViewController(animated: true)
        
        //
    }
    @IBAction func pencilPressed(_ sender: UIButton) {
        guard let pencil = Pencil(tag: sender.tag) else {return}
        color = pencil.color
        if pencil == .eraser {
            opacity = 1.0
        }
    }
    
    //MARK:- Functions
    func startNotesTimer() {
        NotesTimer.shared.startTimer { [weak self] (value, isExpire) in
            guard let `self` = self else { return }
            self.timerTitle = value
            if isExpire {
                self.timeOutNotification()
            }
            
        }
    }
   
    func setBackButton(WithImage name: String) {
        navigationItem.leftBarButtonItem = nil
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 80, height: 31)
        button.setImage(UIImage(named: name), for: .normal)
        button.contentHorizontalAlignment = .leading
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
        navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: button)]
    }
    @objc func backBtn(){
        NotesTimer.shared.stop()
        self.navigationController?.popViewController(animated: true)
    }
    func saveImageToDocumentDirectory(image: UIImage) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileName = NSUUID().uuidString + ".png"
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        print("File PAth is ",fileURL)
        if let data = image.jpegData(compressionQuality: 1.0),!FileManager.default.fileExists(atPath: fileURL.path){
            do {
                try data.write(to: fileURL)
                print("file saved")
            } catch {
                print("error saving file:", error)
            }
        }
    }
    func drawLine(from fromPoint: CGPoint, to toPoint: CGPoint) {
        UIGraphicsBeginImageContext(view.frame.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        tempImageView.image?.draw(in: view.bounds)
        
        context.move(to: fromPoint)
        context.addLine(to: toPoint)
        
        context.setLineCap(.round)
        context.setBlendMode(.normal)
        context.setLineWidth(brushWidth)
        context.setStrokeColor(color.cgColor)
        
        context.strokePath()
        
        tempImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        tempImageView.alpha = opacity
        
        UIGraphicsEndImageContext()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        swiped = false
        lastPoint = touch.location(in: view)
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        swiped = true
        let currentPoint = touch.location(in: view)
        drawLine(from: lastPoint, to: currentPoint)
        lastPoint = currentPoint
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !swiped {
            // draw a single point
            drawLine(from: lastPoint, to: lastPoint)
        }
        UIGraphicsBeginImageContext(mainImageView.frame.size)
        mainImageView.image?.draw(in: view.bounds, blendMode: .normal, alpha: 1.0)
        tempImageView?.image?.draw(in: view.bounds, blendMode: .normal, alpha: opacity)
        mainImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        tempImageView.image = nil
    }
    
    
   
    
}

//MARK:- Extensions
extension AddDrawingViewController: SettingsViewControllerDelegate {
    func settingsViewControllerFinished(_ settingsViewController: SettingsViewController) {
        brushWidth = settingsViewController.brush
        opacity = settingsViewController.opacity
        color = UIColor(red: settingsViewController.red,
                        green: settingsViewController.green,
                        blue: settingsViewController.blue,
                        alpha: opacity)
        dismiss(animated: true)
    }
}
//MARK:- Notification
extension AddDrawingViewController{
//    func notification(){
//                let alertController = UIAlertController(title: "Notification", message: "You can Add details for 20 minutes ", preferredStyle: .alert)
//                    // Create the actions
//                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                        UIAlertAction in
//                        NSLog("OK Pressed")
//                    }
//                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive) {
//                        UIAlertAction in
//                    self.navigationController?.popViewController(animated: true)
//                        NSLog("Cancel Pressed")
//                    }
//
//                    // Add the actions
//                    alertController.addAction(okAction)
//                    alertController.addAction(cancelAction)
//
//                    // Present the controller
//                self.present(alertController, animated: true, completion: nil)
//    }
    func timeOutNotification(){
        NotesTimer.shared.stop()
        guard let image = mainImageView.image else {
            let alertController = UIAlertController(title: "Empty Image?", message: "Write something to save", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                
                
            }
            // Add the actions
            alertController.addAction(okAction)
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            return
        }
        self.delegate.dataReceive(image: image)
        self.navigationController?.popViewController(animated: true)
    }
}
