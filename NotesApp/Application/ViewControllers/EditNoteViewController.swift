//
//  EditNoteViewController.swift
//  NotesApp
//
//  Created by Muhammd Taha on 10/03/2021.
//

import UIKit
import AVKit

class EditNoteViewController: UIViewController {
    //MARK:- Outlets
    @IBOutlet weak var txtNote: UITextView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var controlLbl: UILabel!
    @IBOutlet weak var uiSwitch: UISwitch!
    @IBOutlet weak var noteAddress: UITextView!
    
    //MARK:- Initializations
    var control:Int16!
    var data:NotesModel!
    
    //MARK:- LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getValue()
        setBackButton(WithImage: "back24p")
        self.control = data.isControl
        navigationItem.title = "Text Entry"
        
    }
    //MARK:- Actions
    
    @IBAction func uiSwitch(_ sender: UISwitch) {
        if sender.isOn{
            self.controlLbl.text = "Yes"
            control = 1
        }
        else{
            self.controlLbl.text = "No"
            control = 0
        }
    }
    @IBAction func btnSaveData(_ sender: Any) {
        setValues()
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Functions
    
    func editNote(note:NotesModel){
        do {
            try CoreDataManager.manager.updateNote(note: note)
            print("Note updated Succcessfully")
        }catch{
            print("Error updating note")
        }
    }
    
    func setBackButton(WithImage name: String) {
        navigationItem.leftBarButtonItem = nil
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 80, height: 31)
        button.setImage(UIImage(named: name), for: .normal)
        button.contentHorizontalAlignment = .leading
        button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
        button.tintColor = UIColor.white
        navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: button)]
    }
    @objc func backBtn(){
        self.navigationController?.popViewController(animated: true)
    }
    func getValue(){
        self.txtName.text = data.name
        self.txtName.textColor = UIColor.secondryColor
        self.txtEmail.text = data.email
        self.txtEmail.textColor = UIColor.secondryColor
        self.txtNote.text = data.note
        self.txtNote.textColor = UIColor.secondryColor
        self.noteAddress.text = data.noteAddress
        self.txtPhoneNumber.text = data.phoneNumber
        self.txtPhoneNumber.textColor = UIColor.secondryColor
        if data.isControl == 1{
            controlLbl.text = "YES"
            uiSwitch.isOn = true
        }
        else {
            controlLbl.text = "NO"
            uiSwitch.isOn = false
        }
    }
    func setValues(){
        data.name = txtName.text!
        data.email = txtEmail.text!
        data.phoneNumber = txtPhoneNumber.text!
        data.note = txtNote.text
        data.noteAddress = noteAddress.text
        data.isControl = control
        print("Is control",data.isControl)
        editNote(note: data)
        
    }
}

    //MARK:- Extensions
