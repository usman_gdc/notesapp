//
//  AddNoteViewController.swift
//  NotesApp
//
//  Created by Muhammd Taha on 08/03/2021.
//

import UIKit

class AddNoteViewController: UIViewController {
    //MARK:- Outlets
    

    @IBOutlet weak var noteAddress: UITextView!
    @IBOutlet weak var txtNote: UITextView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var controlLbl: UILabel!
    
    //MARK:- Initializations
    var isNotify = true
    var isControl : Int16 = 1
    var currentDate:String!
    var hours: Int = 0
    var mins: Int = 0
    var secs: Int = 0
    
    
    var rightBarButton: UIBarButtonItem!
    var timerTitle: String? = nil {
        didSet {
            guard let _rightBarButton = self.rightBarButton else {
                rightBarButton = UIBarButtonItem.init(title: self.timerTitle ?? "00:00", style: .plain, target: self, action: nil)
                self.navigationItem.rightBarButtonItems =  [rightBarButton]
                return
            }
            _rightBarButton.title = timerTitle ?? ""
        }
    }
    //MARK:- LifeCycles
   
    override func viewDidLoad() {
        super.viewDidLoad()
        startNotesTimer()
        navigationItem.title = "Text Entry"
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Light", size: 34)!]
        timerTitle = nil
        setBackButton(WithImage: "back24p")
        txtNote.layer.cornerRadius = 10
        btn.layer.cornerRadius = 14
        btn.clipsToBounds = true
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    //MARK:- Actions
    @IBAction func uiSwitch(_ sender: UISwitch) {
        if sender.isOn{
            self.controlLbl.text = "Yes"
            self.isControl = 1
        }
        else{
            self.controlLbl.text = "No"
            self.isControl = 0
        }
    }
    
    @IBAction func btnSaveNote(_ sender: Any) {
        let noteModel: NotesModel = NotesModel(email: txtEmail.text, name: txtName.text, noteId: UUID().uuidString, note: txtNote.text, phoneNumber: txtPhoneNumber.text, isArchive: 0, noteType: NoteType.text, mediaUrl: "",isControl: self.isControl, noteAddress: noteAddress.text)
        do {
            try CoreDataManager.manager.addNote(note: noteModel)
        } catch {
            print("Exception occur")
        }
        NotesTimer.shared.stop()
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Functions
    func startNotesTimer() {
        NotesTimer.shared.startTimer { [weak self] (value, isExpire) in
            guard let `self` = self else { return }
            self.timerTitle = value
            if(self.isNotify){
                self.notification(value: value)
                self.isNotify = false
            }
            if isExpire == true {
                self.timeExpireNotification()
            }
        }
    }
}
//MARK:- Notification
extension AddNoteViewController{
    
    func notification(value :String){
        let alertController = UIAlertController(title: "Notification", message: "You can Add details for \(value) minutes ", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
           // Time.shared.startCountdown(title: self.timerTitle!)
            NSLog("OK Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    func timeExpireNotification(){
        let alertController = UIAlertController(title: "Time expire", message: "You have completed your 20 minutes to add notes ", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
//            let noteModel: NotesModel = NotesModel(email: self.txtEmail.text, name: self.txtName.text, noteId: UUID().uuidString, note: self.txtNote.text, phoneNumber: self.txtPhoneNumber.text, isArchive: 0, noteType: NoteType.text, mediaUrl: "",isControl: self.isControl)
//            do {
//                try CoreDataManager.manager.addNote(note: noteModel)
//            } catch {
//                print("Exception occur")
//            }
            self.navigationController?.popViewController(animated: true)
            NSLog("OK Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
}
//MARK:- Back Button

extension AddNoteViewController{
    func setBackButton(WithImage name: String) {
        navigationItem.leftBarButtonItem = nil
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 80, height: 31)
        button.setImage(UIImage(named: name), for: .normal)
        button.tintColor = UIColor.white
        button.contentHorizontalAlignment = .leading
        button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
        navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: button)]
    }
    @objc func backBtn(){
        NotesTimer.shared.stop()
        self.navigationController?.popViewController(animated: true)
    }
}

