//
//  ViewController.swift
//  NotesApp
//
//  Created by Muhammd Taha on 08/03/2021.
//
import MessageUI
import UIKit

enum noteType{
    case text
    case voice
    case image
}
class HomeViewController: UIViewController {
    
    //MARK:- Outlets

    @IBOutlet weak var backgroundLabel: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = UITableView.automaticDimension
          //  tableView.estimatedRowHeight = 100
        }
    }
    
    //MARK:- Initializations
    var currentDate : String!
    var noteDate : String!
    var dataSource = [NotesModel]()
    
    //MARK:- LifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        NotesTimer.shared.checkTimerIsLocked()
        registerNibs()
        navigationItem.title = "My Notes"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.actionSheet))
        navigationController?.navigationBar.barTintColor = UIColor.secondryColor
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
    }
    override func viewWillDisappear(_ animated: Bool) {
//        DispatchQueue.main.async {
//            self.localNotify()
//        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.getNotes()
    }
    
    //MARK:- Actions
    
    //MARK:- Functions
    
//
//    func localNotify(){
//
//            let content = UNMutableNotificationContent()
//            content.title = NSString.localizedUserNotificationString(forKey: "Notes APP!", arguments: nil)
//            content.body = NSString.localizedUserNotificationString(forKey: "You have created last note 24 hours ago , You can add new note now", arguments: nil)
//            content.sound = UNNotificationSound.default
//            content.categoryIdentifier = "notify-test"
//        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 84600, repeats: false)
//            let request = UNNotificationRequest.init(identifier: "notify-test", content: content, trigger: trigger)
//            let center = UNUserNotificationCenter.current()
//        center.add(request)
//    }
    
    func getDocumentsDirectory() -> URL{
     let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
     let documentsDirectory = paths[0]
     print("The file is saved at", documentsDirectory)
     return documentsDirectory
 }
    func getNotes() {
        do {
            dataSource = try CoreDataManager.manager.getAllNotes()
                self.tableView.reloadData()
            
        } catch {
            print("Exception occur")
        }
    }
    
    func deleteNoteBy(noteId: String) {
        do {
            try CoreDataManager.manager.deleteNoteBy(noteId: noteId)
            self.dataSource = try CoreDataManager.manager.getAllNotes()
            DispatchQueue.main.async{
                self.tableView.reloadData()
            }
        } catch {
        print("Error Catched")
        }
    }
    func registerNibs(){
        tableView.register(UINib(nibName: "HomeTableCell", bundle: nil), forCellReuseIdentifier: "HomeTableCell")
    }
    
    @objc func shareSheet(note: String,subject:String){
        let actionSheet = UIAlertController(title: "Share Your note", message: "",  preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Send via SMS", style: .default, handler: { (action: UIAlertAction) in
            
            guard MFMessageComposeViewController.canSendText() else {
                print("An error Occured in messeging")
                return
            }
            let messageVC = MFMessageComposeViewController()
            messageVC.body = note
                           messageVC.recipients = ["Enter Recipients Here"]
                           messageVC.messageComposeDelegate = self
                           self.present(messageVC, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Send via Email", style: .default, handler: { (action: UIAlertAction) in
            guard MFMailComposeViewController.canSendMail() else {
                print("An error Occured in mailing")
                return
            }
            let composer = MFMailComposeViewController()
            composer.mailComposeDelegate = self
            composer.setToRecipients(["support@seanallen.co"])
              composer.setSubject(subject)
            composer.setMessageBody(note, isHTML: false)
            
            self.present(composer, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction) in
            
        }))
        actionSheet.popoverPresentationController?.sourceView = self.view
        actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        self.present(actionSheet, animated: true, completion: nil)
    }
    @objc func actionSheet(){
        
            let actionSheet = UIAlertController(title: "Add new Note", message: "",  preferredStyle: .actionSheet)

            actionSheet.addAction(UIAlertAction(title: "Text", style: .default, handler: { (action: UIAlertAction) in
                let viewController = self.storyboard?.instantiateViewController(identifier: "AddNoteViewController") as! AddNoteViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }))
            actionSheet.addAction(UIAlertAction(title: "Voice", style: .default, handler: { (action: UIAlertAction) in
                let viewController = self.storyboard?.instantiateViewController(identifier: "AddVoiceViewController") as! AddVoiceViewController

                self.navigationController?.pushViewController(viewController, animated: true)
            }))
            actionSheet.addAction(UIAlertAction(title: "Drawing", style: .default, handler: { (action: UIAlertAction) in
                let viewController = self.storyboard?.instantiateViewController(identifier: "DrawingMainViewController") as! DrawingMainViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }))
        actionSheet.addAction(UIAlertAction(title: "Archive Notes", style: .default, handler: { (action : UIAlertAction) in
            let viewController = self.storyboard?.instantiateViewController(identifier: "ArchiveNotesViewController") as! ArchiveNotesViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }))
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction) in
            }))
            
            actionSheet.popoverPresentationController?.sourceView = self.view
            actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            self.present(actionSheet, animated: true, completion: nil)
        }
    func editNote(noteId:String,isArchive:Int16){
        do {
            try CoreDataManager.manager.updatearch(noteId: noteId, isArchive: Int(isArchive))
            
            print("Archive Sccessfully")
        }catch{
            print("Error in Archiving")
            }
        }
}

//MARK:- Table Delegetes

extension HomeViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource.count == 0{
            backgroundLabel.isHidden = false
        }
        else{
            backgroundLabel.isHidden = true
        }
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell", for: indexPath) as! HomeTableCell
        switch self.dataSource[indexPath.row].noteType!{
        case .text:
            cell.comminit(name: dataSource[indexPath.row].name!, note:dataSource[indexPath.row].note!)
        case .voice:
            cell.comminit(name: dataSource[indexPath.row].name!, note:dataSource[indexPath.row].email!)
        case .drawing:
            cell.comminit(name: dataSource[indexPath.row].name!, note:dataSource[indexPath.row].note!)
        }
//        cell.comminit(name: dataSource[indexPath.row].name!, note:dataSource[indexPath.row].note!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("at Current clicked array", dataSource[indexPath.row])
        switch self.dataSource[indexPath.row].noteType!{
        case .text:
            let viewConroller = self.storyboard?.instantiateViewController(identifier: "EditNoteViewController") as! EditNoteViewController
            viewConroller.data = dataSource[indexPath.row]
            self.navigationController?.pushViewController(viewConroller, animated: true)
        case .voice:
            let viewConroller = self.storyboard?.instantiateViewController(identifier: "EditVoiceViewController") as! EditVoiceViewController
            viewConroller.data = dataSource[indexPath.row]
            self.navigationController?.pushViewController(viewConroller, animated: true)
        case .drawing:
            let viewConroller = self.storyboard?.instantiateViewController(identifier: "EditDrawingMainViewController") as! EditDrawingMainViewController
            viewConroller.data = dataSource[indexPath.row]
            self.navigationController?.pushViewController(viewConroller, animated: true)
        }
     }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, completionHandler in
            self.deleteNoteBy(noteId: self.dataSource[indexPath.row].noteId!)
        }
        let shareAction = UIContextualAction(style: .destructive, title: "Share") { [self] _, _, completionHandler in
            shareSheet(note: self.dataSource[indexPath.row].note!, subject: self.dataSource[indexPath.row].name!)
        }
        shareAction.backgroundColor = .systemBlue
        deleteAction.backgroundColor = .systemRed
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction,shareAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let archiveAction = UIContextualAction(style: .destructive, title: "Archive") { _, _, completionHandler in
            self.dataSource[indexPath.row].isArchive = 1
           self.editNote(noteId: self.dataSource[indexPath.row].noteId!, isArchive: self.dataSource[indexPath.row].isArchive!)
            self.getNotes()
            //self.tableView.reloadData()
            
        }
        archiveAction.backgroundColor = .systemGreen
        let configuration = UISwipeActionsConfiguration(actions: [archiveAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
    
    
}
//MARK:- MSG delegete
extension HomeViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message Sent!")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
}
extension HomeViewController:MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let _ = error {
            //Show error alert
            controller.dismiss(animated: true)
            return
        }
        switch result {
        case .cancelled:
            print("Cancelled")
        case .failed:
            print("Failed to send")
        case .saved:
            print("Saved")
        case .sent:
            print("Email Sent")
        @unknown default:
            break
        }
        controller.dismiss(animated: true)
    }
    
}
/**
//MARK:- Outlets
//MARK:- Initializations
//MARK:- LifeCycles
//MARK:- Actions
//MARK:- Functions
//MARK:- Extensions
 */
