//
//  ArchiveNotesViewController.swift
//  NotesApp
//
//  Created by Muhammd Taha on 07/04/2021.
//

import UIKit

class ArchiveNotesViewController: UIViewController {
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = UITableView.automaticDimension
        }
    }
    
    
    //MARK:- Initializations
    var dataSource = [NotesModel]()
    
    //MARK:- LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Archieve Notes"
        registerNibs()
        self.setBackButton(WithImage: "back24p")
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getNotes()
    }
    
    //MARK:- Actions
    
    
    //MARK:- Functions
    func getNotes() {
        do {
            dataSource = try CoreDataManager.manager.getArchiveNotes()     
                self.tableView.reloadData()
        } catch {
            print("Exception occur")
        }
    }
    func registerNibs(){
        tableView.register(UINib(nibName: "HomeTableCell", bundle: nil), forCellReuseIdentifier: "HomeTableCell")
    }
    func setBackButton(WithImage name: String) {
        navigationItem.leftBarButtonItem = nil
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 80, height: 31)
        button.setImage(UIImage(named: name), for: .normal)
        button.contentHorizontalAlignment = .leading
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
        navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: button)]
    }
    
    @objc func backBtn(){
        self.navigationController?.popViewController(animated: true)
    }
    func editNote(noteId:String,isArchive:Int16){
        do {
            try CoreDataManager.manager.updatearch(noteId: noteId, isArchive: Int(isArchive))
            print("Archive Sccessfully")
        }catch{
            print("Error in Archiving")
            }
        }
}

    //MARK:- Extensions
extension ArchiveNotesViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell", for: indexPath) as! HomeTableCell
        cell.comminit(name: dataSource[indexPath.row].name!, note:dataSource[indexPath.row].note!)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("at Current clicked array", dataSource[indexPath.row])
        switch self.dataSource[indexPath.row].noteType!{
        case .text:
            let viewConroller = self.storyboard?.instantiateViewController(identifier: "EditNoteViewController") as! EditNoteViewController
            viewConroller.data = dataSource[indexPath.row]
            self.navigationController?.pushViewController(viewConroller, animated: true)
        case .voice:
            let viewConroller = self.storyboard?.instantiateViewController(identifier: "EditVoiceViewController") as! EditVoiceViewController
            viewConroller.data = dataSource[indexPath.row]
            self.navigationController?.pushViewController(viewConroller, animated: true)
        case .drawing:
            let viewConroller = self.storyboard?.instantiateViewController(identifier: "EditDrawingMainViewController") as! EditDrawingMainViewController
            viewConroller.data = dataSource[indexPath.row]
            self.navigationController?.pushViewController(viewConroller, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let archiveAction = UIContextualAction(style: .destructive, title: "UnArchive") { _, _, completionHandler in
            self.dataSource[indexPath.row].isArchive = 0
            self.editNote(noteId: self.dataSource[indexPath.row].noteId!, isArchive: self.dataSource[indexPath.row].isArchive!)
            self.getNotes()
           // self.tableView.reloadData()
        }
        archiveAction.backgroundColor = .systemGreen
        let configuration = UISwipeActionsConfiguration(actions: [archiveAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
    
}
    

