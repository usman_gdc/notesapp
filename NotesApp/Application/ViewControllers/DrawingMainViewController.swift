//
//  DrawingMainViewController.swift
//  NotesApp
//
//  Created by Muhammd Taha on 10/03/2021.
//

import UIKit

class DrawingMainViewController: UIViewController{
    //MARK:- Outlets
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtMail: UITextField!
    @IBOutlet weak var txtNote: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageBtn: UIButton!
    @IBOutlet weak var controlLbl: UILabel!
    
    
    //MARK:- Initializations
    var isNofify = true
    var isControl : Int16 = 1
    var imageUrl : String!
    var urlDrawing:UIImage!
    var hours: Int = 0
    var mins: Int = 0
    var secs: Int = 0
    var rightBarButton: UIBarButtonItem!
    var timerTitle: String? = nil {
        didSet {
            guard let _rightBarButton = self.rightBarButton else {
                rightBarButton = UIBarButtonItem.init(title: self.timerTitle ?? "00:00", style: .plain, target: self, action: nil)
                self.navigationItem.rightBarButtonItems =  [rightBarButton]
                self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
                return
            }
            _rightBarButton.title = timerTitle ?? ""
        }
    }
    
    //MARK:- LifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startNotesTimer()
        navigationItem.title = "Drawing Note"
        self.setBackButton(WithImage: "back24p")
    }
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    //MARK:- Actions
    @IBAction func uiSwitch(_ sender: UISwitch) {
        if sender.isOn{
            self.controlLbl.text = "Yes"
            self.isControl = 1
        }
        else{
            self.controlLbl.text = "No"
            self.isControl = 0
        }
    }
    @IBAction func btnSave(_ sender: Any) {
        self.saveImageToDocumentDirectory(image: imageView.image!)
        NotesTimer.shared.stop()
        let noteModel: NotesModel = NotesModel(email: txtMail.text, name: txtName.text, noteId: UUID().uuidString, note: txtNote.text, phoneNumber: txtPhone.text, isArchive: 0, noteType: NoteType.drawing, mediaUrl:imageUrl,isControl: self.isControl, noteAddress:"")
        do {
            try CoreDataManager.manager.addNote(note: noteModel)
            self.navigationController?.popViewController(animated: true)
        } catch {
            print("Exception occur")
        }
    }
    @IBAction func btnImage(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(identifier: "AddDrawingViewController") as! AddDrawingViewController
        viewController.delegate = self
        NotesTimer.shared.stop()
        viewController.mainImageView = imageView
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //MARK:- Functions
    func startNotesTimer() {
        NotesTimer.shared.startTimer { [weak self] (value, isExpire) in
            guard let `self` = self else { return }
            self.timerTitle = value
            if(self.isNofify){
                self.notification(value:value)
                self.isNofify = false
            }
            
            if isExpire {
                self.timeExpireNotification()
            }
            
        }
    }
   
        func setBackButton(WithImage name: String) {
            navigationItem.leftBarButtonItem = nil
            let button = UIButton(type: .custom)
            button.frame = CGRect(x: 0, y: 0, width: 80, height: 31)
            button.setImage(UIImage(named: name), for: .normal)
            button.contentHorizontalAlignment = .leading
            button.tintColor = UIColor.white
            button.tintColor = UIColor.white
            button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
            navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: button)]
        }
    @objc func backBtn(){
        NotesTimer.shared.stop()
        self.navigationController?.popViewController(animated: true)
    }
    func saveImageToDocumentDirectory(image: UIImage) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileName = NSUUID().uuidString + ".png"
        
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        print("File PAth is ",fileURL)
        if let data = image.jpegData(compressionQuality: 1.0),!FileManager.default.fileExists(atPath: fileURL.path){
            do {
                try data.write(to: fileURL)
                imageUrl = fileURL.lastPathComponent
                print("Path of file as string is ", self.imageUrl!)
            } catch {
                print("error saving file:", error)
            }
        }
    }
}
    //MARK:- Delegate Extensions

extension DrawingMainViewController: CanReceive{
    func dataReceive(image: UIImage) {
        self.imageView.image = image
    }
}

//MARK:- Notification Extension
extension DrawingMainViewController{
    func timeExpireNotification(){
        let alertController = UIAlertController(title: "Time expire", message: "You have completed your 20 minutes to add notes ", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
//            let noteModel: NotesModel = NotesModel(email: self.txtMail.text, name: self.txtName.text, noteId: UUID().uuidString, note: self.txtNote.text, phoneNumber: self.txtPhone.text, isArchive: 0, noteType: NoteType.drawing, mediaUrl:self.imageUrl,isControl: self.isControl)
//            do {
//                try CoreDataManager.manager.addNote(note: noteModel)
//            } catch {
//                print("Exception occur")
//            }
            self.navigationController?.popViewController(animated: true)
            NSLog("OK Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    func notification(value : String){
        let alertController = UIAlertController(title: "Notification", message: "You can Add details for \(value) minutes ", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
}
