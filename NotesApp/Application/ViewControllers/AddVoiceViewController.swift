//  AddVoiceViewController.swift
//  NotesApp
//  Created by Muhammd Taha on 08/03/2021.

import UIKit
import AVFoundation
import AVKit

class AddVoiceViewController: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    //MARK:- Outlets
    
    @IBOutlet weak var detailTime: UILabel!
    @IBOutlet weak var detailPlayBtn: UIButton!
    @IBOutlet weak var detailRecordBtn: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtMail: UITextField!
    @IBOutlet weak var playbtn: UIButton!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var recordingBtn: UIButton!
    @IBOutlet weak var controlLbl: UILabel!
    
    //MARK:- Initialization
    var isControl : Int16 = 1
    var isNotify = true
    var audioRecorder: AVAudioRecorder!
    var audioPlayer : AVAudioPlayer!
    var meterTimer : Timer!
    var isAudioRecordingGranted: Bool!
    var isRecording = false
    var isPlaying = false
    var rightBarButton: UIBarButtonItem!
    var urlAudio : String!
    var urlDetailAudio : String!  //
    var recording: Recording!
    var fileName : String = ""
    var audioName : String = "" //
    var hours: Int = 0
    var mins: Int = 0
    var secs: Int = 0
    var timerTitle: String? = nil {
        didSet {
            guard let _rightBarButton = self.rightBarButton else {
                rightBarButton = UIBarButtonItem.init(title: self.timerTitle ?? "00:00", style: .plain, target: self, action: nil)
                self.navigationItem.rightBarButtonItems =  [rightBarButton]
                return
            }
            _rightBarButton.title = timerTitle ?? ""
        }
    }
    
    //MARK:- LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        checkRecordPermission()
        startNotesTimer()
        navigationItem.title = "Voice Note"
        timerTitle = nil
        self.setBackButton(WithImage: "back24p")
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
       
    }
    
    //MARK:- Actions
    @IBAction func btnPlayDetail(_ sender: Any) {
        if(isPlaying){
            audioPlayer.stop()
            detailRecordBtn.isEnabled = true
            detailPlayBtn.tintColor = UIColor.black
            isPlaying = false
        }
        else{
            if FileManager.default.fileExists(atPath: setMediaUrl().path){
                detailRecordBtn.isEnabled = false
                detailPlayBtn.tintColor = UIColor.red
                play()
                audioPlayer.play()
                meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.detailPlayMeter(timer:)), userInfo:nil, repeats:true)
                isPlaying = true
            }
            else{
                print("setFileUrl().absoluteString ",setMediaUrl().path)
                print("Audio File is Missing")
            }
        }
    }
    @IBAction func btnRecordDetail(_ sender: Any) {
        if(isRecording){
            finishAudioRecording(success: true)
            detailRecordBtn.tintColor = UIColor.black
            detailPlayBtn.isEnabled = true
            isRecording = false
        }
        else{
            setRecorder()

            audioRecorder.record()
            meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.detailRecordMeter(timer:)), userInfo:nil, repeats:true)
            detailRecordBtn.tintColor = UIColor.red
            detailPlayBtn.isEnabled = false
            isRecording = true
        }
    }
    @IBAction func uiSwitch(_ sender: UISwitch) {
        if sender.isOn{
            self.controlLbl.text = "Yes"
            self.isControl = 1
        }
        else{
            self.controlLbl.text = "No"
            self.isControl = 0
        }
    }
    @IBAction func btnSave(_ sender: Any) {
        let noteModel: NotesModel = NotesModel(email: txtMail.text, name: txtName.text, noteId: UUID().uuidString, note:"", phoneNumber: txtPhone.text, isArchive: 0, noteType: NoteType.voice, mediaUrl: urlAudio,isControl: self.isControl, noteAddress: "",addressUrl: self.urlDetailAudio)
        do {
            try CoreDataManager.manager.addNote(note: noteModel)
            NotesTimer.shared.stop()
            self.navigationController?.popViewController(animated: true)
        } catch {
            print("Exception occur")
        }
    }
    @IBAction func playAudioBtn(_ sender: Any) {
        if(isPlaying){
            audioPlayer.stop()
            recordingBtn.isEnabled = true
            playbtn.tintColor = UIColor.black
            isPlaying = false
        }
        else{
            if FileManager.default.fileExists(atPath: setFileUrl().path){
                recordingBtn.isEnabled = false
                playbtn.tintColor = UIColor.red
                prepare_play()
                audioPlayer.play()
                meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updatePlayerMeter(timer:)), userInfo:nil, repeats:true)
                isPlaying = true
            }
            else{
                print("setFileUrl().absoluteString ",setFileUrl().path)
                print("Audio File is Missing")
            }
        }
    }
    @IBAction func startRecordingBtn(_ sender: Any) {
        if(isRecording){
            finishAudioRecording(success: true)
            recordingBtn.tintColor = UIColor.black
            playbtn.isEnabled = true
            isRecording = false
        }
        else{
            setupRecorder()

            audioRecorder.record()
            meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updateAudioMeter(timer:)), userInfo:nil, repeats:true)
            recordingBtn.tintColor = UIColor.red
            playbtn.isEnabled = false
            isRecording = true
        }
    }
    //MARK:- Functions
    
    func startNotesTimer() {
        NotesTimer.shared.startTimer { [weak self] (value, isExpire) in
            guard let `self` = self else { return }
            self.timerTitle = value
            if(self.isNotify){
                self.notification(value: value)
                if(self.isNotify){
                    self.notification(value: value)
                    self.isNotify = false
                }
            }
            if isExpire {
                self.timeExpireNotification()
            }
        }
    }
    func setBackButton(WithImage name: String) {
        navigationItem.leftBarButtonItem = nil
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 80, height: 31)
        button.setImage(UIImage(named: name), for: .normal)
        button.contentHorizontalAlignment = .leading
        button.tintColor = UIColor.white
        
        button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
        navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: button)]
    }
    
    @objc func backBtn(){
        NotesTimer.shared.stop()
        self.navigationController?.popViewController(animated: true)
    }
    @objc func updateAudioMeter(timer: Timer){
        if audioRecorder.isRecording{
            //     let hr = Int((audioRecorder.currentTime / 60) / 60)
            let min = Int(audioRecorder.currentTime / 60)
            let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
            let totalTimeString = String(format: "%02d:%02d", min, sec)
            timeLbl.text = totalTimeString
            audioRecorder.updateMeters()
        }
    }
    @objc func updatePlayerMeter(timer: Timer){
        if audioPlayer.isPlaying{
         //   let hr = Int((audioPlayer.currentTime / 60) / 60)
            let min = Int(audioPlayer.currentTime / 60)
            let sec = Int(audioPlayer.currentTime.truncatingRemainder(dividingBy: 60))
            let totalTimeString = String(format: "%02d:%02d", min, sec)
            timeLbl.text = totalTimeString
            audioPlayer.updateMeters()
        }
    }
    func finishAudioRecording(success: Bool){
        if success{
            audioRecorder.stop()
            audioRecorder = nil
            meterTimer.invalidate()
            print("recorded successfully.")
        }
        else{
            print("Error Recording failed")
        }
    }
    func checkRecordPermission(){
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSessionRecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSessionRecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (allowed) in
                if allowed {
                    self.isAudioRecordingGranted = true
                } else {
                    self.isAudioRecordingGranted = false
                }
            })
            break
        default:
            break
        }
    }
    func getDocumentsDirectory() -> URL{
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        print("getDocumentsDirectory() -> URL", documentsDirectory)
        return documentsDirectory
    }
    func setFileUrl() ->URL{
        if urlAudio == nil{
            print("No audio file Exist")
        }
           let filePath = getDocumentsDirectory().appendingPathComponent(urlAudio)
            print( "setFileUrl() ->URL  " , filePath)
            return filePath
    }
    func getFileUrl() -> URL{
        let  filename = NSUUID().uuidString + ".m4a"
        self.fileName = filename
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
        urlAudio = filename
        print( "getFileUrl() -> URL " ,urlAudio!)
        return filePath
    }
    func setupRecorder(){
        if isAudioRecordingGranted{
            let session = AVAudioSession.sharedInstance()
            do{
                try session.setCategory(AVAudioSession.Category.playAndRecord, options: .defaultToSpeaker)
                try session.setActive(true)
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
                ]
                audioRecorder = try AVAudioRecorder(url: getFileUrl(), settings: settings)
                audioRecorder.delegate = self
                audioRecorder.isMeteringEnabled = true
                audioRecorder.prepareToRecord()
            }
            catch let error {
                print("Error is " , error)
            }
        }
        else
        {
            print("You have not accesed by Microphone")
        }
    }
    func prepare_play(){
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        if let documentDirectoryURL: NSURL = urls.first as NSURL? {
         print(documentDirectoryURL)// File copied to this directory
            let soundURL = documentDirectoryURL.appendingPathComponent(fileName)
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL!)
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            //audioPlayer.play()
        }
        catch{
            print("Error")
            }
        }
    
    
    }
}
//MARK:- Details Recorder
extension AddVoiceViewController{
    func setRecorder(){
        if isAudioRecordingGranted{
            let session = AVAudioSession.sharedInstance()
            do{
                try session.setCategory(AVAudioSession.Category.playAndRecord, options: .defaultToSpeaker)
                try session.setActive(true)
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
                ]
                audioRecorder = try AVAudioRecorder(url: getmediaUrl(), settings: settings)
                audioRecorder.delegate = self
                audioRecorder.isMeteringEnabled = true
                audioRecorder.prepareToRecord()
            }
            catch let error {
                print("Error is " , error)
            }
        }
        else
        {
            print("You have not accesed by Microphone")
        }
    }
    //    func setFileUrl() ->URL{
    //        if urlAudio == nil{
    //            print("No audio file Exist")
    //        }
    //        let filePath = getDocumentsDirectory().appendingPathComponent(urlAudio)
    //            print( "setFileUrl() ->URL  " , filePath)
    //            return filePath
    //    }
    func setMediaUrl() ->URL{
        if  urlDetailAudio == nil{
            print("No audio file Exist")
        }
        let filePath = getDocumentsDirectory().appendingPathComponent(urlDetailAudio)
            print( "setFileUrl() ->URL  " , filePath)
            return filePath
    }
    func getmediaUrl() -> URL{
        let filename = NSUUID().uuidString + ".m4a"
        self.audioName = filename
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
        self.urlDetailAudio = filename
        print( "getFileUrl() -> URL " ,self.urlDetailAudio!)
        return filePath
    }
    @objc func detailRecordMeter(timer: Timer){
        if audioRecorder.isRecording{
            //     let hr = Int((audioRecorder.currentTime / 60) / 60)
            let min = Int(audioRecorder.currentTime / 60)
            let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
            let totalTimeString = String(format: "%02d:%02d", min, sec)
            detailTime.text = totalTimeString
            audioRecorder.updateMeters()
        }
    }
    @objc func detailPlayMeter(timer: Timer){
        if audioPlayer.isPlaying{
         //   let hr = Int((audioPlayer.currentTime / 60) / 60)
            let min = Int(audioPlayer.currentTime / 60)
            let sec = Int(audioPlayer.currentTime.truncatingRemainder(dividingBy: 60))
            let totalTimeString = String(format: "%02d:%02d", min, sec)
            detailTime.text = totalTimeString
            audioPlayer.updateMeters()
        }
    }
    func play(){
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        if let documentDirectoryURL: NSURL = urls.first as NSURL? {
         print(documentDirectoryURL)// File copied to this directory
            let soundURL = documentDirectoryURL.appendingPathComponent(audioName)
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL!)
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            //audioPlayer.play()
        }
        catch{
            print("Error")
            }
        }
    
    
    }
    
}
//MARK:- Notification
extension AddVoiceViewController{
    func notification(value: String){
        let alertController = UIAlertController(title: "Notification", message: "You can Add details for \(value) minutes ", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }

    func timeExpireNotification(){
        let alertController = UIAlertController(title: "Time expire", message: "You have completed your 20 minutes to add notes ", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
           
//            let noteModel: NotesModel = NotesModel(email: self.txtMail.text, name: self.txtName.text, noteId: UUID().uuidString, note: self.txtNote.text, phoneNumber: self.txtPhone.text, isArchive: 0, noteType: NoteType.voice, mediaUrl: self.urlAudio,isControl: self.isControl)
//            do {
//                try CoreDataManager.manager.addNote(note: noteModel)
//            } catch {
//                print("Exception occur")
//            }
            self.navigationController?.popViewController(animated: true)
            NSLog("OK Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
}
