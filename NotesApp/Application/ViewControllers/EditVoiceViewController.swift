//  EditVoiceViewController.swift
//  NotesApp
//
//  Created by Muhammd Taha on 10/03/2021.

import UIKit
import AVFoundation
import AVKit

class EditVoiceViewController: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    //MARK:- Outlets
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtMail: UITextField!
    @IBOutlet weak var playbtn: UIButton!
    @IBOutlet weak var recordingBtn: UIButton!
    @IBOutlet weak var lblStartTimer: UILabel!
    @IBOutlet weak var lblTotalTime: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var uiSwitch: UISwitch!
    @IBOutlet weak var isControl: UILabel!
    @IBOutlet weak var detailTime: UILabel!
    @IBOutlet weak var detailPlayBtn: UIButton!
    @IBOutlet weak var detailRecordBtn: UIButton!
    
    //MARK:- Initializations
    var control:Int16!
    var data:NotesModel!
    var isRecording = false
    var audioRecorder: AVAudioRecorder!
    var meterTimer : Timer!
    var isAudioRecordingGranted: Bool!
    var isPlaying = false
    var audioPlayer : AVAudioPlayer!
    var urlAudio:String!
    var urlDetailAudio : String!  //
    var audioName : String = "" //
    
    var hours: Int = 0
    var mins: Int = 0
    var secs: Int = 0
    
    //MARK:- LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        getValue()
        checkRecordPermission()
        setBackButton(WithImage: "back24p")
        self.control = data.isControl
        navigationItem.title = "Edit Voice Note"
    }
    
    //MARK:- Actions
    @IBAction func uiSwitch(_ sender: UISwitch) {
        if sender.isOn{
            self.isControl.text = "Yes"
            control = 1
        }
        else{
            self.isControl.text = "No"
            control = 0
        }
    }
    @IBAction func btnRecordDetails(_ sender: Any) {
        if(isRecording){
            finishAudioRecording(success: true)
            detailRecordBtn.tintColor = UIColor.black
            detailPlayBtn.isEnabled = true
            isRecording = false
        }
        else{
            setRecorder()

            audioRecorder.record()
            meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.detailRecordMeter(timer:)), userInfo:nil, repeats:true)
            detailRecordBtn.tintColor = UIColor.red
            detailPlayBtn.isEnabled = false
            isRecording = true
        }
        
    }
    @IBAction func btnPlayDetails(_ sender: Any) {
        if(isPlaying){
            audioPlayer.stop()
            detailRecordBtn.isEnabled = true
            detailPlayBtn.tintColor = UIColor.black
            isPlaying = false
        }
        else{
            if FileManager.default.fileExists(atPath: setMediaUrl().path){
                detailRecordBtn.isEnabled = false
                detailPlayBtn.tintColor = UIColor.red
                play()
                audioPlayer.play()
                meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.detailPlayMeter(timer:)), userInfo:nil, repeats:true)
                isPlaying = true
            }
            else{
                print("setFileUrl().absoluteString ",setMediaUrl().path)
                print("Audio File is Missing")
            }
        }
    }
    
    @IBAction func btnStartRecording(_ sender: Any) {
        if(isRecording){
            finishAudioRecording(success: true)
            recordingBtn.tintColor = UIColor.black
            playbtn.isEnabled = true
            isRecording = false
        }
        else{
            setupRecorder()
            audioRecorder.record()
            meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updateAudioMeter(timer:)), userInfo:nil, repeats:true)
            recordingBtn.tintColor = UIColor.red
            playbtn.isEnabled = false
            isRecording = true
        }
    }
    @IBAction func btnPlayRecording(_ sender: Any) {
        
        if(isPlaying){
            audioPlayer.stop()
            playbtn.setImage(UIImage(named: "play"), for: .normal)
            isPlaying = false
        }
        else{
            if FileManager.default.fileExists(atPath: setFileUrl().path){
                playbtn.setImage(UIImage(named: "pause"), for: .normal)
                prepare_play()
                audioPlayer.play()
                meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updatePlayerMeter(timer:)), userInfo:nil, repeats:true)
                isPlaying = true
            }
            else{
                print("Audio File is Missing")
            }
        }
    }
    @IBAction func btnSave(_ sender: Any) {
        setValues()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDelFile(_ sender: Any) {
        
        if self.data.mediaUrl == nil{
            btnDelete.isHidden = false
        }
        else{
            removeFile(itemName: data.mediaUrl!)
            print("Url of audio is ",data.mediaUrl!)
            btnDelete.isHidden = true
        }
    }
    
    //MARK:- Functions
    func setBackButton(WithImage name: String) {
        navigationItem.leftBarButtonItem = nil
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 80, height: 31)
        button.setImage(UIImage(named: name), for: .normal)
        button.contentHorizontalAlignment = .leading
        button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
        button.tintColor = UIColor.white
        navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: button)]
    }
    @objc func backBtn(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func updatePlayerMeter(timer: Timer){
        if audioPlayer.isPlaying{
            let min = Int(audioPlayer.currentTime / 60)
            let sec = Int(audioPlayer.currentTime.truncatingRemainder(dividingBy: 60))
            let totalTimeString = String(format: "%02d:%02d", min, sec)
            lblStartTimer.text = totalTimeString
            audioPlayer.updateMeters()
        }
    }
    func prepare_play(){
        do
        {
            audioPlayer = try AVAudioPlayer(contentsOf: setFileUrl())
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
        }
        catch{
            print("Error")
        }
    }
    func removeFile(itemName:String) {
      let fileManager = FileManager.default
      let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
      let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
      let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
      guard let dirPath = paths.first else {
          return
      }
      let filePath = "\(dirPath)/\(itemName)"
      do {
        try fileManager.removeItem(atPath: filePath)
        print("File Removed Successfully")
      } catch let error as NSError {
        print("Error is ==== ", error.debugDescription)
        print("File Removed Error")
        
      }
    }
    
    func removeFile(noteUrl:String){
        
        let fileManager = FileManager.default
        do {
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let filePaths = try fileManager.contentsOfDirectory(atPath: "\(String(describing:noteUrl))")
            for filePath in filePaths {
                try fileManager.removeItem(atPath:filePath)
                print("file removed At path",filePath)
            }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    func editNote(note:NotesModel){
        do {
            try CoreDataManager.manager.updateNote(note: note)
            print("Note updated Succcessfully")
        }catch{
            print("Error updating note")
        }
    }
    func getValue(){
        self.txtName.text = data.name
        self.txtName.textColor = UIColor.primaryColor
        self.txtMail.text = data.email
        self.txtMail.textColor = UIColor.primaryColor
        self.txtPhone.text = data.phoneNumber
        self.txtPhone.textColor = UIColor.primaryColor
        self.urlDetailAudio = data.addressUrl
        self.urlAudio = data.mediaUrl
        if data.isControl == 1{
            isControl.text = "YES"
            uiSwitch.isOn = true
        }
        else {
            isControl.text = "NO"
            uiSwitch.isOn = false
        }
    }
    func setValues(){
        data.name = txtName.text!
        data.email = txtMail.text!
        data.phoneNumber = txtPhone.text!
        data.mediaUrl = self.urlAudio
        data.addressUrl = self.urlDetailAudio
        data.isControl = control
        print("Is control",data.isControl)
        editNote(note: data)
    }
    func finishAudioRecording(success: Bool){
        if success{
            audioRecorder.stop()
            audioRecorder = nil
            meterTimer.invalidate()
            print("recorded successfully.")
        }
        else{
            print("Error Recording failed")
        }
    }
    func checkRecordPermission(){
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSessionRecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSessionRecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (allowed) in
                if allowed {
                    self.isAudioRecordingGranted = true
                } else {
                    self.isAudioRecordingGranted = false
                }
            })
            break
        default:
            break
        }
    }
    func setupRecorder(){
        if isAudioRecordingGranted{
            let session = AVAudioSession.sharedInstance()
            do
            {
                try session.setCategory(AVAudioSession.Category.playAndRecord, options: .defaultToSpeaker)
                try session.setActive(true)
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
                ]
                audioRecorder = try AVAudioRecorder(url: getFileUrl(), settings: settings)
                audioRecorder.delegate = self
                audioRecorder.isMeteringEnabled = true
                audioRecorder.prepareToRecord()
            }
            catch let error {
                print("Error is " , error)
            }
        }
        else{
            print("You have not accesed by Microphone")
        }
    }
    func getDocumentsDirectory() -> URL{
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        print("The file is saved at", documentsDirectory)
        return documentsDirectory
    }
    func setFileUrl() ->URL{
        let filePath = getDocumentsDirectory().appendingPathComponent(data.mediaUrl!)
        print( " New Path of file is " , filePath)
        return filePath
    }
    func getFileUrl() -> URL{
        let filename = NSUUID().uuidString + ".m4a"
        urlAudio = filename
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
        print( "Path of file is " , filePath)
        
        return filePath
    }
    @objc func updateAudioMeter(timer: Timer){
        if audioRecorder.isRecording{
            let min = Int(audioRecorder.currentTime / 60)
            let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
            let totalTimeString = String(format: "%02d:%02d",  min, sec)
            lblStartTimer.text = totalTimeString
            audioRecorder.updateMeters()
        }
    }
}
//MARK:- Extensions
extension EditVoiceViewController{
    func setMediaUrl() ->URL{
        if  urlDetailAudio == nil{
            print("No audio file Exist")
        }
        let filePath = getDocumentsDirectory().appendingPathComponent(urlDetailAudio)
            print( "setFileUrl() ->URL  " , filePath)
            return filePath
    }
    func setRecorder(){
        if isAudioRecordingGranted{
            let session = AVAudioSession.sharedInstance()
            do{
                try session.setCategory(AVAudioSession.Category.playAndRecord, options: .defaultToSpeaker)
                try session.setActive(true)
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
                ]
                audioRecorder = try AVAudioRecorder(url: getmediaUrl(), settings: settings)
                audioRecorder.delegate = self
                audioRecorder.isMeteringEnabled = true
                audioRecorder.prepareToRecord()
            }
            catch let error {
                print("Error is " , error)
            }
        }
        else
        {
            print("You have not accesed by Microphone")
        }
    }
    func getmediaUrl() -> URL{
        let filename = NSUUID().uuidString + ".m4a"
        self.audioName = filename
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
        self.urlDetailAudio = filename
        print( "getFileUrl() -> URL " ,self.urlDetailAudio!)
        return filePath
    }
    @objc func detailRecordMeter(timer: Timer){
        if audioRecorder.isRecording{
            //     let hr = Int((audioRecorder.currentTime / 60) / 60)
            let min = Int(audioRecorder.currentTime / 60)
            let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
            let totalTimeString = String(format: "%02d:%02d", min, sec)
            detailTime.text = totalTimeString
            audioRecorder.updateMeters()
        }
    }
    @objc func detailPlayMeter(timer: Timer){
        if audioPlayer.isPlaying{
         //   let hr = Int((audioPlayer.currentTime / 60) / 60)
            let min = Int(audioPlayer.currentTime / 60)
            let sec = Int(audioPlayer.currentTime.truncatingRemainder(dividingBy: 60))
            let totalTimeString = String(format: "%02d:%02d", min, sec)
            detailTime.text = totalTimeString
            audioPlayer.updateMeters()
        }
    }
    func play(){
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        if let documentDirectoryURL: NSURL = urls.first as NSURL? {
         print(documentDirectoryURL)// File copied to this directory
            print("URL of Audio ",audioName)
            let soundURL = documentDirectoryURL.appendingPathComponent(audioName)
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL!)
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            //audioPlayer.play()
        }
        catch{
            print("Error")
            }
        }
    }
}
