//
//  AppConstants.swift
//  NotesApp
//
//  Created by Muhammd Taha on 04/03/2021.
//

import Foundation
import UIKit

class AppConstants{
    // MARK: - Singleton
    static let shared = AppConstants()
    
    func notification(){
        let alertController = UIAlertController(title: "Notification", message: "You can Add details for 20 minutes ", preferredStyle: .alert)

            // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
            self.startCountdown()
                NSLog("OK Pressed")
            }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive) {
                UIAlertAction in
            self.dismiss(animated: true, completion: nil)
                NSLog("Cancel Pressed")
            }

            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)

            // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
}
