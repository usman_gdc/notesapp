//
//  HomeTableCell.swift
//  NotesApp
//
//  Created by Muhammd Taha on 08/03/2021.
//

import UIKit

class HomeTableCell: UITableViewCell {

    //MARK:- Outlets
   
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var noteLbl: UILabel!
    
  
    
    //MARK:- Initializations
    
    
    //MARK:- Lifecyles
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLbl.tintColor = UIColor.secondryColor
        noteLbl.tintColor = UIColor.secondryColor
    }
    override func layoutSubviews() {
        super.layoutSubviews()

//        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5))
    }
    
    //MARK:- Functions
   
    func comminit(name:String , note:String){
        nameLbl.text = name
        noteLbl.text = note
        
    }
}

